<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use getID3;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    public function create(Request $request, getID3 $id3_analyzer)
    {
        $this->validate($request, [
            'video' => 'required',
        ]);

        $info = $id3_analyzer->analyze($request->file('video'));

        if (isset($info['error'])) {
            return response($info, 422);
        }

        return json_encode($info, JSON_PARTIAL_OUTPUT_ON_ERROR);
    }
}

<?php

use Illuminate\Http\UploadedFile;

class VideosTest extends TestCase
{
    public function testPostVideosReturnsID3Json()
    {
        $file_name = 'big_buck_bunny_720p_1mb.mp4';
        $file      = new UploadedFile(base_path("tests/test_files/{$file_name}"), $file_name);

        $response = $this->call('POST', 'v1/videos', [], [], ['video' => $file]);

        $this->assertEquals(200, $response->status());

        $response_json = json_decode($response->content(), true);

        $this->assertArraySubset([
            'filesize'        => 1055736,
            'audio'           => ['dataformat' => 'mp4'],
            'playtime_string' => '0:05',
        ], $response_json);
    }

    public function testPostVideosWithoutVideoFile()
    {
        $response = $this->call('POST', 'v1/videos');

        $this->assertEquals(422, $response->status());
    }

    public function testPostVideosWithInvalidVideoFile()
    {
        $file_name = 'invalid_file.txt';
        $file      = new UploadedFile(base_path("tests/test_files/{$file_name}"), $file_name);

        $response = $this->call('POST', 'v1/videos', [], [], ['video' => $file]);

        $this->assertEquals(422, $response->status());
    }
}
